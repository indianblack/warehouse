![Project image](images/warehouse-img.png)

# Simple warehouse

SpringBoot & Thymeleaf web application that handles a simple warehouse. The application should store data in memory and show the status of products in stock.

## Basic requirements
1. The main page displays all available products along with basic information: name, serial number, quantity and price for one product.
2. After selecting a specific product, we redirect to the page with additional information. This page should display all the information from the home page and additionally a description of the product.
3. It should be possible to edit the number of products in the warehouse - by clicking on the action on the home page (plus/minus).
4. Additionally: it should be possible to add new products - HTML form.

## Instructions
1. Clone this repository.
2. Run project in IDE.
3. Open http://localhost:8080/ in browser.

## Useful links
- Generate getters/setters IntelliJ: https://stackoverflow.com/questions/16988504/intellij-code-completion-for-all-setter-getter-methods-of-local-variable-object
- Warehouse products: https://www.warehouseproductsstore.com/
- Get item details: https://stackoverflow.com/questions/54229021/how-to-select-a-list-item-by-id-using-thymeleaf-and-direct-to-another-detail-p
- How to find an element in a list: https://www.baeldung.com/find-list-element-java
- Thymeleaf namespace unknown: https://smarterco.de/intellij-idea-thymeleaf-namespace-unkown/ 
- Thymeleaf input number: https://frontbackend.com/thymeleaf/spring-boot-bootstrap-thymeleaf-input-number
- Thymeleaf field value: https://stackoverflow.com/questions/25027801/how-to-set-thymeleaf-thfield-value-from-other-variable
- Button onclick event Thymeleaf: https://stackoverflow.com/questions/58070607/how-to-set-a-button-onclick-event-and-link-to-thymeleaf-controller
- HTTP response status codes: https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
- Free vector illustrations: https://icons8.com/illustrations
- Bootstrap: https://getbootstrap.com/
- Thymeleaf button onclick: https://stackoverflow.com/questions/32994746/thymeleaf-onclick-send-td-value-to-javascript-function
- Thymeleaf button onclick in table: https://stackoverflow.com/questions/38546100/can-i-make-http-post-request-from-thymeleaf-table-in-spring-boot-application

## License
[MIT](https://choosealicense.com/licenses/mit/)